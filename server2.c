/* A simple server in the internet domain using TCP */
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h> 
#include <sys/socket.h>
#include <netinet/in.h>
#include <signal.h>
#include <string.h>
#include <sys/time.h> 

#include <unistd.h>
#include <errno.h>

void dostuff(int); /* function prototype */

/* This function is called when a system call fails, 
 * it will display on stderr and then abort */
void error(char *msg)
{
    perror(msg);
    exit(1);
}

int main()
{
     /* The following keeps zombies at bay. 
      * When children die and the parent doesn't wait() on
      * them then their SIGCHLD handler is not happy because
      * the process is not permitted to fully die because at
      * some point in the future, the parent of the process might
      * want to execute a wait and would want info on the death of
      * the child. */

     signal(SIGCHLD,SIG_IGN);

     int sockfd, newsockfd, portno, clilen, pid;
     portno = 4949;

     /* sockfd and newsockfd are file descriptors containing 
      * the values returned by the socket and accept syscall 
      * portno stores the port number the server will accept 
      * connections, clilen stores the size of the address of 
      * the client, this is needed for the accept syscall, 
      * n is the return value for the read() and write() calls,
      * which will be the number of characters read or written.
      */

      /* serv_addr will contain the address of the server, and
       * cli_addr will contain the address of the client connecting */
      
     struct sockaddr_in serv_addr, cli_addr;

     /* Create a new socket */
     sockfd = socket(AF_INET, SOCK_STREAM, 0);
     if (sockfd < 0) 
        error("ERROR opening socket");
     
     /* zero out the serv_addr buffer */
     bzero((char *) &serv_addr, sizeof(serv_addr));

     /* set serv_addr structure fields:
      * sin_family = code for the address family
      * sin_port = contains the port number (converted to network byte order)
      * in_addr = contains s_addr = IP address of the server */
     serv_addr.sin_family = AF_INET;
     serv_addr.sin_port = htons(portno);
     serv_addr.sin_addr.s_addr = INADDR_ANY;

     /* bind() binds a socket to an address, takes socket fd, address to bind
      * size of the address to which it is bound */
     if (bind(sockfd, (struct sockaddr *) &serv_addr,
              sizeof(serv_addr)) < 0) 
              error("ERROR on binding");

     /* listen on the socket for connections */
     listen(sockfd,5);

     /* accept() causes the process to block until client connects, wakes up 
      * when a connection from a client has succeeded and then returns a new
      * file descriptor, and all communications should be done on this fd.
      * The second argument is a reference pointer to the address of the client
      * on the other end of the connection, third arg is the size of the structure. */
     clilen = sizeof(cli_addr);

     while (1) {
     	newsockfd = accept(sockfd, 
		(struct sockaddr *) &cli_addr, 
                &clilen);
     	if (newsockfd < 0) 
          	error("ERROR on accept");
	pid = fork();
	if (pid <0)
		error("ERROR on fork");
	if (pid == 0) {
		close (sockfd);
		dostuff(newsockfd);
		exit(0);
	}
	else close (newsockfd);
     } /* end of while loop */
     return 0; /* never gets here */
}

/******** DOSTUFF() *********************
 * There is a separate instance of this function 
 * for each connection.  It handles all communication
 * once a connnection has been established.
 ******************************************/

void dostuff (int sock)
{
	int n;

	/* Server reads characters from the socket connection into
         * this buffer */
     	char buffer[256];

	char* nodemsg;
	nodemsg = "birdbath.riseup.net\n.\n";

	char* version;
	version = "fakemunins node on birdbath.riseup.net version: 1.2.4\n";

	char* timeoutmsg;
	timeoutmsg = "Timeout!\n";

	char* errormsg;
	errormsg = "# Unknown command. Try list, nodes, config, fetch, version or quit\n";

	char* greeting;
	greeting = "# munin node at birdbath.riseup.net\n";

     	n = write(sock, greeting, strlen(greeting));
	
	int loop;
	loop=1;

	while (loop) {

	  bzero(buffer,256);
	  n = read(sock,buffer,255);
	  buffer[n+1] = '\0';

	  if (n < 0) error("ERROR reading from socket");

	  if(strcmp(buffer, "list\r\n") == 0) {
	    FILE *fpipe;
	    char *command="ls /etc/munin/plugins";
	    char line[256];
	    if ( !(fpipe = (FILE*)popen(command,"r")) ) {
	      perror("Problems with pipe");
	      exit(1);
	    }

	    while ( fgets( line, sizeof line, fpipe))
	      {
		n = write (sock, line, strlen(line));
	      }
	    pclose(fpipe);
	  }
	      
	   	  
	  else if(strcmp(buffer, "nodes\r\n") == 0) {
	    n = write (sock, nodemsg, strlen(nodemsg));
	  }
	  
	  else if(strcmp(buffer, "config\r\n") == 0) {
	    /* spit configs */
	  }

	  else if(strcmp(buffer, "help\r\n") == 0) {
	    n = write (sock, errormsg, strlen(errormsg));
	  }	

	  else if(strcmp(buffer, "version\r\n") == 0) {
	    n = write (sock, version, strlen(version));
	  }	

	  else if(strcmp(buffer, "quit\r\n") == 0) {
	    exit(0);
	  }
	  
	  else {
	    n = write(sock, errormsg, strlen(errormsg));
	  }
	}
	
     	/* n = write(sock,"I got your message",18); */
     	if (n < 0) error("ERROR writing to socket");
}

