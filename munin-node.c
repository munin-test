/* A complicated in the internet domain using TCP */
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h> 
#include <sys/socket.h>
#include <sys/stat.h>
#include <netinet/in.h>
#include <signal.h>
#include <string.h>
#include <sys/time.h> 

#include <dirent.h>
#include <unistd.h>
#include <errno.h>
#include <regex.h>

#define MAX_DIR_STR  4096
#define CONFIGEXPR   "^config (([a-zA-z0-9_]+(\\.)?)+)"
#define FETCHEXPR    "^fetch (([a-zA-z0-9_]+(\\.)?)+)"
#define LISTEXPR    "^list"
#define QUITEXPR    "^quit"

void commands(int);

/* This function is called when a system call fails, 
 * it will display on stderr and then abort */
void error(char *msg)
{
    perror(msg);
    exit(1);
}

int main()
{
     /* The following keeps zombies at bay. 
      * When children die and the parent doesn't wait() on
      * them then their SIGCHLD handler is not happy because
      * the process is not permitted to fully die because at
      * some point in the future, the parent of the process might
      * want to execute a wait and would want info on the death of
      * the child. */

     signal(SIGCHLD,SIG_IGN);

     int sockfd, newsockfd, portno, clilen, pid;
     portno = 4949;

     /* sockfd and newsockfd are file descriptors containing 
      * the values returned by the socket and accept syscall 
      * portno stores the port number the server will accept 
      * connections, clilen stores the size of the address of 
      * the client, this is needed for the accept syscall, 
      * n is the return value for the read() and write() calls,
      * which will be the number of characters read or written.
      */

      /* serv_addr will contain the address of the server, and
       * cli_addr will contain the address of the client connecting */
      
     struct sockaddr_in serv_addr, cli_addr;

     /* Create a new socket */
     sockfd = socket(AF_INET, SOCK_STREAM, 0);
     if (sockfd < 0) 
        error("ERROR opening socket");

     u_int yes = 1;
     if (setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR, &yes, sizeof(yes)) < 0) {
	     error("can't reuse, lame.");
	     return;
     }
     
     /* zero out the serv_addr buffer */

     bzero((char *) &serv_addr, sizeof(serv_addr));

     /* set serv_addr structure fields:
      * sin_family = code for the address family
      * sin_port = contains the port number (converted to network byte order)
      * in_addr = contains s_addr = IP address of the server */
     serv_addr.sin_family = AF_INET;
     serv_addr.sin_port = htons(portno);
     serv_addr.sin_addr.s_addr = INADDR_ANY;

     /* bind() binds a socket to an address, takes socket fd, address to bind
      * size of the address to which it is bound */
     if (bind(sockfd, (struct sockaddr *) &serv_addr,
              sizeof(serv_addr)) < 0) 
              error("ERROR on binding");

     /* listen on the socket for connections */
     listen(sockfd,5);

     /* accept() causes the process to block until client connects, wakes up 
      * when a connection from a client has succeeded and then returns a new
      * file descriptor, and all communications should be done on this fd.
      * The second argument is a reference pointer to the address of the client
      * on the other end of the connection, third arg is the size of the structure. */
     clilen = sizeof(cli_addr);

     while (1) {
     	newsockfd = accept(sockfd, 
		(struct sockaddr *) &cli_addr, 
                &clilen);
     	if (newsockfd < 0) 
          	error("ERROR on accept");
	pid = fork();
	if (pid <0)
		error("ERROR on fork");
	if (pid == 0) {
		close (sockfd);
		commands(newsockfd);
		exit(0);
	}
	else close (newsockfd);
     } /* end of while loop */
     return 0; /* never gets here */
}

/******** commands() *********************
 * There is a separate instance of this function 
 * for each connection.  It handles all communication
 * once a connnection has been established.
 ******************************************/

void commands(int sock)
{
	int n;

	/* Server reads characters from the socket connection into
         * this buffer */
     	char buffer[256];

	char* nodemsg;
	nodemsg = "birdbath.riseup.net\n.\n";

	char* version;
	version = "fakemunins node on birdbath.riseup.net version: 1.2.4\n";

	char* timeoutmsg;
	timeoutmsg = "Timeout!\n";

	char* errormsg;
	errormsg = "# Unknown command. Try list, nodes, config, fetch, version or quit\n";

	char* greeting;
	greeting = "# munin node at birdbath.riseup.net\n";

	char* unknownservice;
	unknownservice = "# Unknown service\n.\n";

	char* debug;
	debug = "# HI YOU GOT TEH DEBEUG!!?!\n";

	char* fetch;
	fetch = "# fetch message\n";

	char* eofmsg;
	eofmsg = ".\n";

     	n = write(sock, greeting, strlen(greeting));
	
	int loop;
	loop=1;

	/* To be able to match regular expressions, we need this, 
	 * one for "config" and one for "fetch"
	 */
	 
	regex_t config_re;
	if(regcomp(&config_re, CONFIGEXPR, REG_EXTENDED) != 0) {
	  exit (1);
	}

	regex_t fetch_re;
	if(regcomp(&fetch_re, FETCHEXPR, REG_EXTENDED) != 0) {
	  exit (1);
	}

	regex_t list_re;
	if(regcomp(&list_re, LISTEXPR, REG_EXTENDED) != 0) {
	  exit (1);
	}

	regex_t quit_re;
	if(regcomp(&quit_re, QUITEXPR, REG_EXTENDED) != 0) {
	  exit (1);
	}

	char ret[MAX_DIR_STR];
	char* dir;

	while (loop) {

	  bzero(buffer,256);
	  n = read(sock,buffer,255);
	  buffer[n+1] = '\0';

	  if (n < 0) error("ERROR reading from socket");

	  if((regexec(&list_re, buffer, 0, NULL, 0)) == 0) {
	    struct dirent *dp;
	    DIR *dfd;
	    ret[0] = 0;
	    char* list;

	    if ((dfd = opendir("/etc/munin/plugins")) == NULL) {
	      perror(dir);
	      return;
	    }

	    while ((dp = readdir(dfd)) !=NULL) {
	      
	      /* skip self and parent */
	      if (strcmp(dp->d_name,  ".") == 0 || 
		  strcmp(dp->d_name, "..") == 0) {
		continue;
	      }
	      
	      if (strlen(ret) + strlen(dp->d_name) + 2 > MAX_DIR_STR) {
		closedir(dfd);
		strcpy(ret, "too long");
		return;
	      } else {
		strcat(ret, strcat(dp->d_name, " "));
	      }
	    }
	    ret[strlen(ret) - 1] = 0;
	    closedir(dfd);
	    strcat(ret, "\n");
	    n = write (sock, ret, strlen(ret));

	  }	      
	   	  
	  else if(strcmp(buffer, "nodes\r\n") == 0) {
	    n = write (sock, nodemsg, strlen(nodemsg));
	  }

	  /* do a a regexp to see if "config bar" was sent 
	   * check if second arg is a plugin, otherwise print 
	   * "# Unknown service\n." */

	  else if((regexec(&config_re, buffer, 0, NULL, 0)) == 0) {
	    struct stat statinfo;
	    char *p = strchr(buffer, ' '); /* Now p will point to the space between config and bar */
	    char *service = p+1; /* put whatever is after that space into *service */
	    char *servicedir = malloc(sizeof(char) * 1024); /* allocate some memory to store everything */
	    char *plugin_path = "/etc/munin/plugins"; /* where the plugins are located */

	    /* Do a little overflow checking */
	    if ((strlen(plugin_path) + strlen(service) + 3) > 1024) {
	      error("path too long");
	      return;
	    }

	    /* Prepend the plugin path into the beginning of the service name */
	    sprintf(servicedir, "%s/%s", plugin_path, service);
	    
	    /* Remove the \r\n at the end of the string */
	    if (servicedir[strlen(servicedir) -1 ] == '\n') {
	      if (servicedir[strlen(servicedir) -2 ] == '\r') {
		servicedir[strlen(servicedir) - 2] = 0;
	      }
	      else {
		servicedir[strlen(servicedir) - 1] = 0;
	      }
	    }
	    
	    /* what the string has 
	     char*temp = servicedir; while(*temp){printf("%c - %d\n", *temp, (int)*temp);temp++;} 
	    */

	    /* Check to see if the service entered exists in the directory */
	    if(stat(servicedir, &statinfo) == -1) {
	      n = write(sock, unknownservice, strlen(unknownservice));
	    }
	    /* Since it does exist, check to make sure it is a regular file or a link */
	    else if(statinfo.st_mode & (S_IFREG || S_IFLNK)) {
	      FILE *fpipe;
	      char results[256];
	      char *command=(strcat(servicedir, " config"));

	      if ( !(fpipe = (FILE*)popen(command,"r")) )
		{  // If fpipe is NULL
		  perror("Problems with pipe");
		  exit(1);
		}

	      while ( fgets( results, sizeof results, fpipe))
		{
		  n = write(sock, results, strlen(results));    
		}
	      pclose(fpipe);
	      n = write(sock, eofmsg, strlen(eofmsg));
	    }
	    /* gotta free up this memory because servicedir is malloc()'d */
	    free(servicedir);
	  }

	  /* do a a regexp to see if "fetch bar" was sent 
	   * check if second arg is a plugin, otherwise print 
	   * "# Unknown service\n." */

	  else if((regexec(&fetch_re, buffer, 0, NULL, 0)) == 0) {
	    struct stat statinfo;
	    char *p = strchr(buffer, ' '); /* Now p will point to the space between config and bar */
	    char *service = p+1; /* put whatever is after that space into *service */
	    char *servicedir = malloc(sizeof(char) * 1024); /* allocate some memory to store everything */
	    char *plugin_path = "/etc/munin/plugins"; /* where the plugins are located */

	    /* Do a little overflow checking */
	    if ((strlen(plugin_path) + strlen(service) + 3) > 1024) {
	      error("path too long");
	      return;
	    }

	    /* Prepend the plugin path into the beginning of the service name */
	    sprintf(servicedir, "%s/%s", plugin_path, service);
	    
	    /* Remove the \r\n at the end of the string */
	    if (servicedir[strlen(servicedir) -1 ] == '\n') {
	      if (servicedir[strlen(servicedir) -2 ] == '\r') {
		servicedir[strlen(servicedir) - 2] = 0;
	      }
	      else {
		servicedir[strlen(servicedir) - 1] = 0;
	      }
	    }
	    
	    /* Check to see if the service entered exists in the directory */
	    if(stat(servicedir, &statinfo) == -1) {
	      n = write(sock, unknownservice, strlen(unknownservice));
	    }
	    /* Since it does exist, check to make sure it is a regular file or a link */
	    else if(statinfo.st_mode & (S_IFREG || S_IFLNK)) {
	      FILE *fpipe;
	      char results[256];
	      char *command=(strcat(servicedir, " fetch"));

	      if ( !(fpipe = (FILE*)popen(command,"r")) )
		{  // If fpipe is NULL
		  perror("Problems with pipe");
		  exit(1);
		}

	      while ( fgets( results, sizeof results, fpipe))
		{
		  n = write(sock, results, strlen(results));    
		}
	      pclose(fpipe);
	      n = write(sock, eofmsg, strlen(eofmsg));
	    }
	    /* gotta free up this memory because servicedir is malloc()'d */
	    free(servicedir);
	  }
	  
	  else if(strcmp(buffer, "help\r\n") == 0) {
	    n = write (sock, errormsg, strlen(errormsg));
	  }	

	  else if(strcmp(buffer, "version\r\n") == 0) {
	    n = write (sock, version, strlen(version));
	  }	

	  else if((regexec(&quit_re, buffer, 0, NULL, 0)) == 0) {
	    exit(0);
	  }
	  
	  else {
	    n = write(sock, errormsg, strlen(errormsg));
	  }
	}
	
     	if (n < 0) error("ERROR writing to socket");
}

